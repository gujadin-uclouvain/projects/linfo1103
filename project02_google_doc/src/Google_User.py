#! /usr/bin/python3
# -*- coding: utf-8 -*-

from Google_Doc import Google_Doc
from Google_Log import Google_Log
from Google_Line import Google_Line

class Google_User:
    """
    Cette classe représente un utilisateur qui contribue à un GoogleDoc.

    Attributs:
        name (`str`): Nom de l'utilisateur
        history (dictionnary of pairs (`str`, 'Google_Log')): Dictionnaire qui associe des paires d'identifiants uniques (clé) des documents aux logs (valeurs) `Google_Log` correspondants
    """
    def __init__(self, name):
        """
        :pre: `name` est un `str` non null
        :post: construit un nouvel utilisateur avec un nom et historique personnel
        """
        self.name = name # Nom de l'utilisateur
        self.history = {} # Dictionnaire de paires (clé:valeurs) où les clés sont les identifiants uniques des documents et les valeurs sont les Google_Log() associés.


    def add_line(self, doc, location, new_line):
        """
        :pre: `doc` est un objet `Google_Doc` valide
        :pre: `location` est un `int` >= 0
        :pre: `new_line` est un `str`
        :post:  - si `location` >= len(`doc`), une ligne est ajoutée à la fin du `Google_Doc`, sinon la ligne est
                    ajoutée à l'indice `location` et les indices des lignes suivantes sont incrémentés de un
                - la modification est sauvée dans l'historique de l'utilisateur pour restauration du document sans la ligne
                - l'utilisateur est ajouté à l'attribut `users` du documents
                - la modification n'interfère pas avec (= n'empêche pas) les modifications et restaurations des autres lignes
        """
        doc.users.add(self.name)
        if doc.doc_id not in self.history:
            self.history[doc.doc_id] = Google_Log()
        log = self.history[doc.doc_id]

        inst_new_line = Google_Line(new_line.strip())
        doc.lines.insert(location, inst_new_line)
        id_line = inst_new_line.getID()

        log.push("add", location, id=id_line)


    def remove_line(self, doc, location):
        """
        :pre: `doc` est un objet `Google_Doc` valide
        :pre: `location` est un `int` >= 0
        :post:  - si `location` >= len(`doc`), ne fait rien, sinon:
                - une ligne du `Google_Doc` est supprimée
                - la modification est sauvée dans l'historique de l'utilisateur pour restauration avec la ligne
                - l'utilisateur est ajouté à l'attribut `users` du documents
                - la modification n'interfère pas avec (= n'empêche pas) les modifications et restaurations des autres lignes
        """
        doc.users.add(self.name)
        if doc.doc_id not in self.history:
            self.history[doc.doc_id] = Google_Log()
        log = self.history[doc.doc_id]

        if location < len(doc):
            old_line = doc.lines[location]
            doc.lines.pop(location)

            log.push("remove", location, old_content=old_line)


    def edit_line(self, doc, location, new_line):
        """
        :pre: `doc` est un objet `Google_Doc` valide
        :pre: `location` est un `int` >= 0
        :pre: new_line est un `str`
        :post:  - si `location` >= len(`doc`), ne fait rien, sinon:
                - la ligne à l'indice `location` est remplacée par une nouvelle ligne de texte `new_line`
                - la modification est sauvée dans l'historique de l'utilisateur pour restauration à la version initiale de la ligne
                - l'utilisateur est ajouté à l'attribut `users` du documents
                - la modification n'interfère pas avec (= n'empêche pas) les modifications et restaurations des autres lignes
        """
        doc.users.add(self.name)
        if doc.doc_id not in self.history:
            self.history[doc.doc_id] = Google_Log()
        log = self.history[doc.doc_id]

        if location < len(doc):
            old_line = doc.lines[location]

            doc.lines.pop(location)
            inst_new_line = Google_Line(new_line.strip())
            doc.lines.insert(location, inst_new_line)
            id_line = inst_new_line.getID()

            log.push("edit", location, id=id_line, old_content=old_line)


    def restore(self, doc):
        """
        :pre: `doc` est un objet `Google_Doc` valide
        :post:  - la dernière modification de l'utilisateur est annulée et le document reviens à la version avant modification pour autant que ce soit possible
                - la restauration est considérée possible si :
                    - la version de la ligne après modification est présente (par exemple, si une ligne "a1" a été éditée en une ligne "a2", la restauration ne peut être faite que si la ligne correspond à la version "a2". Si la ligne a été supprimée ou si elle est devenue "a3", la restauration n'est pas possible)
                - si la restauration n'est pas possible, le document n'est pas modifié
                - dans tous les cas, la dernière modification est retirée de l'historique
        """
        log = self.history[doc.doc_id]
        if log.size > 0:
            backups = log.pull()

            if backups["methode"] == "add":
                actual_line = doc.lines[backups["line"]]
                actual_line_id = actual_line.getID()

                if actual_line_id == backups["id"]:
                    doc.lines.pop(backups["line"])
                else:
                    for index, line in enumerate(doc.lines):
                        if line.getID() == backups["id"]:
                            doc.lines.pop(index)
                            break

            elif backups["methode"] == "remove":
                doc.lines.insert(backups["line"], backups["old_content"])

            elif backups["methode"] == "edit":
                actual_line = doc.lines[backups["line"]]
                actual_line_id = actual_line.getID()

                if actual_line_id == backups["id"]:
                    doc.lines.pop(backups["line"])
                    doc.lines.insert(backups["line"], backups["old_content"])
                else:
                    for index, line in enumerate(doc.lines):
                        if line.getID() == backups["id"]:
                            doc.lines.pop(index)
                            doc.lines.insert(index, backups["old_content"])
                            break
