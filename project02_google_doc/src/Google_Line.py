#! /usr/bin/python3
# -*- coding: utf-8 -*-

class Google_Line:
    """
    La classe Google_Line représente une ligne d'un document Google_Doc.

    Attributs:
        __unique_id (`int`): Variable de classe qui comptabilise le nombre de lignes `Google_Line` crées

    """
    __unique_id = 0

    def __init__(self, txt):
        """
        Attributs:
            __line (`str`): Texte de la ligne
            __id (`int`): Identifiant unique de la ligne
        Args:
            txt (`str`): 
        """
        self.__line = txt
        self.__id = Google_Line.__unique_id
        Google_Line.__unique_id += 1

    def getID(self):
        """
        :post: renvoi l'identifiant unique de la ligne
        """
        return self.__id

    def __len__(self):
        return len(self.__line)

    def __str__(self):
        return self.__line
