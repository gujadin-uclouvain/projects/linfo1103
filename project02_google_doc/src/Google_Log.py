#! /usr/bin/python3
# -*- coding: utf-8 -*-

class Google_Log:
    """
    La classe Google_Log représente l'historique d'un utilisateur Google_User.

    Attributs:
        head (`Node`): Référence vers le dernier noeud créé
        size (`int`): Nombre de noeud dans la classe Google_Log
    """
    class Node:
        """
        La classe Node représente une liste chaînée.

        Attributs:
            elem (`dict`): Backup des modification de la classe Google_Doc sous forme de dictionnaire
            prev (`Node`): Référence vers le noeud précédant
        """
        def __init__(self, elem=None, prev=None):
            """
            :post: assigne à `elem`, None ou un dictionnaire-backup
            :post: assigne à `prev`, None ou une référence vers le noeud précédent
            """
            self.elem = elem
            self.prev = prev

        def value(self):
            """
            :post: renvoi la valeur de elem
            """
            return self.elem

        def get_prev(self):
            """
            :post: renvoi le noeud précédent
            """
            return self.prev


    def __init__(self):
        """
        :post: assigne à head, None ou une référence vers un objet de la classe Node
        """
        self.head = None
        self.size = 0

    def current_value(self):
        """
        :post: renvoi la valeur de elem du dernier noeud ajouté
        """
        return self.head.value()

    def push(self, methode, line, id=None, old_content=None):
        """
        :pre: `methode` est un `str` représentant la méthode utilisée ("add", "remove" ou "edit")
        :pre: `line` est un `int` représentant la ligne où l'élément à été ajouté, retir ou édité
        :pre: `old_content` est un `str` du contenu avant modification
        :post: Ajoute un noeud, (établi un lien vers le noeud précédent) et crée des backups des éléments avant l'exécution des fonctions add, remove et edit
        """
        if self.size == 0:
            self.head = self.Node({"methode": methode, "line": line, "old_content": old_content, "id": id})
        else:
            self.head = self.Node({"methode": methode, "line": line, "old_content": old_content, "id": id}, self.head)
        self.size += 1

    def pull(self):
        """
        :pre: `size` > 0
        :post: Retire le dernier noeud ajouté et renvoi le dictionnaire-backup de ce noeud
        """
        if self.size > 0:
            temp = self.head
            self.head = temp.get_prev()
            self.size -= 1
            return temp.value()
