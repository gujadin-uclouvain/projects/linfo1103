#! /usr/bin/python3
# -*- coding: utf-8 -*-

from Google_Doc import Google_Doc
from Google_User import Google_User
from Google_Line import Google_Line
import unittest


"""
Ce module propose un ensemble de fonctions pour tester votre implémentation.
Une fonction permet de générer un premier document.
Une autre évalue l'égalité de deux documents.

La classe Test_Google_Doc définit un ensemble de méthodes qui permettent de vérifier votre implémentation.
Évidemment, les tests proposés forment une base solide mais incomplète pour la validation de votre code. Vous êtes donc fortement encouragés à développer la classe avec des tests supplémentaires.
"""


def generate_doc_SW(id):
    """
    :pre: `id` est un entier qui identifie un test
    :post: renvoi un `Google_Doc` sur le thème de Star Wars
    """
    base_txt =  "La peur est le chemin vers le côté obscur\n"\
                "la peur mène à la colère\n"\
                "la colère mène à la haine\n"\
                "la haine mène à la souffrance."
    return Google_Doc("Star Wars test " + str(id), base_txt)

def doc_equality(doc, to_compare):
    """
    :pre: `doc` et `to_compare` sont deux `Google_Doc`
    :post: renvoi True si les deux documents ont les mêmes lignes, sinon False
    """
    if len(doc) != len(to_compare):
        return False
    lines_doc = doc.lines
    lines_to_compare = to_compare.lines
    for i in range(len(lines_doc)):
        if str(lines_doc[i]) != str(lines_to_compare[i]):
            return False
    return True

class Test_Google_Doc(unittest.TestCase):
    # Ajouts
    def test_line_addition(self):
        """
        Test l'ajout d'une ligne
        """
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)

        its_a_trap = "C'est un piège !"
        u0.add_line(doc, 2, its_a_trap)
        original.lines.insert(2, Google_Line(its_a_trap))

        self.assertTrue(doc_equality(doc, original), "Votre méthode *add_line* ne fonctionne pas comme attendu.")

    def test_end_of_file_line_addition(self):
        """
        Test l'ajout d'une ligne en fin de document
        """
        self.test_line_addition()
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)

        its_a_trap = "C'est un piège !"
        u0.add_line(doc, 4, its_a_trap)
        original.lines.insert(4, Google_Line(its_a_trap))

        self.assertTrue(doc_equality(doc, original), "Votre méthode *add_line* ne fonctionne pas comme attendu. Vous ne gérez pas correctement l'ajout de ligne en fin de fichier.")

    def test_beyond_size_line_addition(self):
        """
        Test l'ajout d'une ligne au delà de la taille du document
        """
        self.test_line_addition()
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)

        its_a_trap = "C'est un piège !"
        u0.add_line(doc, 7, its_a_trap)
        original.lines.insert(4, Google_Line(its_a_trap))

        self.assertTrue(doc_equality(doc, original), "Votre méthode *add_line* ne fonctionne pas comme attendu. Vous ne gérez pas correctement l'ajout de ligne en fin de fichier.")

    # Retraits
    def test_line_removal(self):
        """
        Test la suppression d'une ligne
        """
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)

        u0.remove_line(doc, 2)
        original.lines.pop(2)

        self.assertTrue(doc_equality(doc, original), "Votre méthode *remove_line* ne fonctionne pas comme attendu.")

    def test_beyond_size_line_removal(self):
        """
        Test la suppression d'une ligne au delà de la taille du document
        """
        self.test_line_removal()
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)

        u0.remove_line(doc, 7)

        self.assertTrue(doc_equality(doc, original), "Votre méthode *remove_line* ne fonctionne pas comme attendu. Vous ne gérez pas correctement la suppression d'une ligne au delà des limites du document")

    # Éditions
    def test_line_edition(self):
        """
        Test l'édition d'une ligne
        """
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)

        its_a_trap = "C'est un piège !"
        u0.edit_line(doc, 2, its_a_trap)
        original.lines.pop(2)
        original.lines.insert(2, Google_Line(its_a_trap))

        self.assertTrue(doc_equality(doc, original), "Votre méthode *edit_line* ne fonctionne pas comme attendu.")

    # Restaurations
    def test_restore(self):
        """
        Test la restauration d'une modification antérieure
        """
        self.test_line_addition()
        self.test_line_removal()
        self.test_line_edition()
        doc = generate_doc_SW(0)

        u0 = Google_User(0)

        u0.add_line(doc, 2, "C'est un piège !")
        u0.remove_line(doc, 1)
        u0.edit_line(doc, 2, "Fais-le, ou ne le fais pas ! Il n’y a pas d’essai.")

        u0.restore(doc)
        u0.restore(doc)
        u0.restore(doc)

        self.assertTrue(doc_equality(doc, generate_doc_SW(0)), "Votre méthode *restore* ne fonctionne pas comme attendu.")

    # Restaurations de plusieurs documents
    def test_multiple_docs(self):
        """
        Test la restauration de modifications antérieures sur différent documents
        """
        self.test_line_addition()
        self.test_line_removal()
        self.test_line_edition()
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)
        doc1 = generate_doc_SW(1)

        u0 = Google_User(0)

        u0.remove_line(doc, 1)
        original.lines.pop(1)

        its_a_trap = "C'est un piège !"
        print("*"*60)
        u0.add_line(doc1, 2, its_a_trap)
        u0.edit_line(doc1, 2, "Ce n'est pas un piège !")
        u0.restore(doc1)
        u0.restore(doc1)

        self.assertTrue(doc_equality(doc, original), "Votre implémentation ne gère pas l'utilisation de plusieurs documents pour un seul utilisateur (0).")
        self.assertTrue(doc_equality(doc1, generate_doc_SW(1)), "Votre implémentation ne gère pas l'utilisation de plusieurs documents pour un seul utilisateur (1).")

    def test_empty_log(self):
        """
        Test la restauration d'une modification antérieure lorsque l'utilisateur n'a plus de modification dans son historique
        """
        self.test_line_addition()
        self.test_restore()
        doc = generate_doc_SW(0)

        u0 = Google_User(0)

        its_a_trap = "C'est un piège !"
        u0.add_line(doc, len(doc), its_a_trap)
        u0.restore(doc)
        u0.restore(doc)

        self.assertTrue(doc_equality(doc, generate_doc_SW(0)), "Votre implémentation ne gère pas correctement les tentatives de restaurations lorsque l'historique de l'utilisateur est vide")

    def test_multiple_users(self):
        """
        Test l'interaction des modifications produites par plusieurs utilisateurs
        """
        self.test_restore()
        self.test_line_removal()
        self.test_line_addition()
        self.test_empty_log()
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)
        u1 = Google_User(1)

        its_a_trap = "C'est un piège ! "
        u0.add_line(doc, len(doc), its_a_trap + str(0))
        u0.add_line(doc, len(doc), its_a_trap + str(1))
        original.lines.insert(len(original), Google_Line(its_a_trap + str(0)))
        original.lines.insert(len(original), Google_Line(its_a_trap + str(1)))

        u1.add_line(doc, len(doc), "Que la force soit avec toi !")
        u1.restore(doc)
        u1.restore(doc)

        self.assertTrue(doc_equality(doc, original), "Votre implémentation ne gère pas correctement l'interaction entre plusieurs utilisateurs")

    # Conflits
    def test_conflict_add(self):
        """
        Test l'interaction des modifications produites par plusieurs utilisateurs qui modifient la même ligne
        """
        self.test_restore()
        self.test_line_addition()
        self.test_empty_log()
        self.test_multiple_users()
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)
        u1 = Google_User(1)

        u1.add_line(original, len(original), "Yoda")

        u0.add_line(doc, len(doc), "Que la force soit avec toi !")
        u1.edit_line(doc, len(doc)-1, "Yoda")
        u0.restore(doc)

        self.assertTrue(doc_equality(doc, original), "Votre implémentation ne gère pas correctement les conflits de modification de la même ligne entre plusieurs utilisateurs")

    def test_conflict_edit(self):
        """
        Test l'interaction des modifications produites par plusieurs utilisateurs qui modifient la même ligne
        """
        self.test_restore()
        self.test_line_addition()
        self.test_empty_log()
        self.test_multiple_users()
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)
        u1 = Google_User(1)

        u1.edit_line(original, 0, "Yoda")

        u0.edit_line(doc, 0, "Que la force soit avec toi !")
        u1.edit_line(doc, 0, "Yoda")
        u0.restore(doc)

        self.assertTrue(doc_equality(doc, original), "Votre implémentation ne gère pas correctement les conflits de modification de la même ligne entre plusieurs utilisateurs")


    def test_conflict_index_moved(self):
        """
        Test l'interaction des modifications produites par plusieurs utilisateurs qui modifient l'index d'une ligne
        """
        self.test_restore()
        self.test_line_addition()
        self.test_empty_log()
        self.test_multiple_users()
        doc = generate_doc_SW(0)
        original = generate_doc_SW(0)

        u0 = Google_User(0)
        u1 = Google_User(1)

        u1.add_line(original, 0, "Dark Vador")
        u1.add_line(original, 0, "Anakin Skywalker")

        u0.add_line(doc, 0, "Que la force soit avec toi !")
        u1.add_line(doc, 0, "Dark Vador")
        u1.add_line(doc, 0, "Anakin Skywalker")
        u0.restore(doc)

        self.assertTrue(doc_equality(doc, original), "Votre implémentation ne gère pas correctement les conflits de déplacement d'index d'une ligne entre plusieurs utilisateurs")


if __name__ == '__main__':
    unittest.main()




