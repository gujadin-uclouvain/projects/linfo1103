#! /usr/bin/python3
# -*- coding: utf-8 -*-

from Google_Line import Google_Line

class Google_Doc:
    """
    La classe Google_Doc représente un document partagé.

    Attributs:
        __unique_id (`int`): Variable de classe qui comptabilise le nombre de documents `Google_Doc` crées

        doc_id (`int`): Identifiant unique du document
        title (`str`): Titre du document
        users (set of `Google_User`): Ensemble des utilisateurs (`Google_User`) qui modifient ou ont modifié le document
        lines (list of `Google_Line`): Tableau des lignes qui constituent le document
    """
    __unique_id = 0

    def __init__(self, title, body = ""):
        """
        Args:
            title (`str`): Titre du document
            body (`str`): Contenu initial du document
        """
        self.doc_id = Google_Doc.__unique_id
        Google_Doc.__unique_id += 1
        self.title = title
        self.users = set() # Ensemble des utilisateurs `Google_User` qui ont contribué au document
        self.lines = [] # Tableau des `Google_Line` qui constituent le document
        self.strToLines(body) # Ajout initial de lignes dans le document

    def strToLines(self, txt, splitPattern = "\n"):
        """
        Découpe la chaîne de caractères `txt` à chaque occurrence du `splitPattern`.
        Toutes les sous-chaînes obtenues sont ajoutées comme de nouvelles lignes `Google_Line` au document
        Args:
            txt (`str`): Chaîne de caractères à découper et à ajouter au document
            splitPattern (`str`): Motif qui délimite les parties de texte a découper
        """
        for l in txt.split(splitPattern):
            self.lines.append(Google_Line(l))

    def __len__(self):
        return len(self.lines)

    def __str__(self):
        return "\n" + ('-'*10) + "\n" + "Google_Doc [" + self.title + "] : \n" + "\n".join([str(line) for line in self.lines]) + "\n" + ('-'*10) + "\n"
