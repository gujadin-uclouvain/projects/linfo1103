#! /usr/bin/python3
# -*- coding: utf-8 -*-
#####################################################
# Projet 1 - Algorithmique  [SINFBAC1]  (2018-2019) #
# Réalisé par Guillaume Jadin et Alexandre Verlaine #
#####################################################

"""
Ce module contient les classes et fonctions de bases qui vous seront nécessaires
lors de la réalisation du premier mini-projet du cours d'algorithmique.
En particulier, ce module déclare:
  * La classe abstraite `ArbreBinaire` que vous devez compléter.
  * La fonction `creer_arbre(forme_parenthesee)` que vous devez implementer.
"""

class ArbreBinaire:
    """
    Classe permettant de définir un arbre binaire.
    """
    def __init__(self, parent, value, left=None, right=None):
        """
        :pre: -
        :post: initialize a new Binary Tree with "value" stored at the root node.
        """
        self.value = value  # The value at the root
        self.parent = parent  # A reference to the parent tree
        self.left = left  # A reference to the left subtree
        self.right = right  # A reference to the right subtree

    def simplifier(self, level):
        """
        Simplifie l'arbre pour produire un découpage correspondant au i-ème
        niveau de l'arbre.

        :pre:    L'arbre est valide (il a pu être interprété correctement)
        :pre:    Le level >= 0
        :post:   Renvoie un découpage de la phrase sous forme de string dont les
                 constituants sont séparés par des |
        """
        def simplifier_rec(level, current_level=0, node=self):
            """
            Simplifie récursivement l'arbre pour produire un découpage correspondant au i-ème
            niveau de l'arbre.

            :pre:    L'arbre est valide (il a pu être interprété correctement)
            :pre:    Le level >= 0
            :post:   Renvoie un découpage de la phrase sous forme de string dont les
                     constituants sont séparés par des |
            """
            # Nous analysons les instances créée dans creer_arbre en verifiant si la valeur est un strings (mot)
            # ou un entier (sentiment).
            if type(node.value) == str:
                current_level -= 1
                return node.value

            elif type(node.value) == int:
                next_node = node.left
                if type(next_node.value) == str:
                    return simplifier_rec(level, current_level, next_node)

                else:
                    # Nous divisons la phrase en fonction des niveaux de l'arbre.
                    current_level += 1
                    if current_level <= level:
                        return simplifier_rec(level, current_level, node.left) + "|" + simplifier_rec(level,
                                                                                                      current_level,
                                                                                                      node.right)
                    return simplifier_rec(level, current_level, node.left) + " " + simplifier_rec(level, current_level,
                                                                                                  node.right)

        return simplifier_rec(level)


def creer_arbre(forme_parenthesee):
    """
     Cette fonction permet de créer une instance d'Arbre correspondant à l'arbre
     encodée dans la chaine de caractères en forme parenthésée.

     :pre:  Le paramètre forme_parenthesee est une chaine de caractères qui encode
            un arbre d'analyse valide.
     :post: Renvoie une instance de **votre classe** qui étend la classe Arbre ci-dessous.
            L'instance retournee correspond à ce qui est encodé dans forme_parenthesee.

     :exc:  Si la précondition devait être violée lors de l'appel à cette fonction,
            vous êtes autorisés à lever une exception de type ValueError qui est une
            exception standard du langage Python3.
    """
    check_parsing(forme_parenthesee)
    g = 1
    current_node = None
    while g < len(forme_parenthesee):
        element = forme_parenthesee[g]

        if element == "(":
            g += 1

        elif element == ")":
            if current_node.parent == None:
                return current_node
            current_node = current_node.parent
            g += 1

        elif element == " ":
            g += 1

        elif is_int(element):
            if is_word(forme_parenthesee, g):
                temp = forme_parenthesee[g:].split(")")
                word = temp[0]
                length = len(word)

                if current_node.left == None:
                    current_node.left = ArbreBinaire(current_node, word)
                else:
                    current_node.right = ArbreBinaire(current_node, word)

                g += length

            else:
                new_node = ArbreBinaire(current_node, int(element))

                if current_node == None:
                    current_node = new_node

                elif current_node.left == None:
                    current_node.left = new_node
                    current_node = current_node.left

                else:
                    current_node.right = new_node
                    current_node = current_node.right

                g += 1

        else:
            #ici une première exception est gérée, si les mots/nombres sont collés.
            if forme_parenthesee[g-1] != " ":
                raise ValueError("Les sentiments ne peuvent pas être accolés aux mots/nombres.")

            temp = forme_parenthesee[g:].split(")")
            word = temp[0]
            length = len(word)

            if current_node.left == None:
                current_node.left = ArbreBinaire(current_node, word)
            else:
                current_node.right = ArbreBinaire(current_node, word)

            g += length


##FONCTIONS & CLASSES UTILITAIRES - DEBUT##
def is_int(s):
    """
    Détermine si 's' est un chiffre/nombre.

    :pre: -
    :post: Renvoi True si 's' est un chiffre, False sinon.
    """
    try:
        int(s)
        return True
    except:
        return False


def is_word(forme_parenthesee, index):
    """
    Détermine si un élément de 'forme_parenthesee', en fonction de 'index' est un mot ou un sentiment (un chiffre).

    :pre: Le paramètre forme_parenthesee est une chaine de caractères.
    :pre: 0 <= index < len(forme_parenthesee)
    :post: Renvoi True si l'élément un mot, False si c'est un sentiment.
    """
    new_form = forme_parenthesee[:index].strip()
    if new_form[len(new_form)-1] == "(":
        return False
    return True


def check_parsing(forme_parenthesee):
    """
    Établi des exceptions sur la chaîne de caractère 'forme_parenthesee'.

    :pre: Le paramètre forme_parenthesee est une chaine de caractères.
    :post: Lève un exception ValueError si une des condition établi n'est pas respecté.
    """
    def fct_parenthese_number(forme_parenthesee):
        """
        Compte le nombre de parenthèse ouvrante et fermante dans 'forme_parenthese'.
        Vérifie si le nombre de parenthèses ouvrantes = nombre de parenthèse fermante.

        :pre: Le paramètre forme_parenthesee est une chaine de caractères.
        :post: Renvoi True si le nombre de parenthèses ouvrantes et fermantes sont égales, False sinon.
        """
        parenthese_number = [0, 0]
        for element in forme_parenthesee:
            if element == "(":
                parenthese_number[0] += 1
            elif element == ")":
                parenthese_number[1] += 1

        if parenthese_number[0] == parenthese_number[1]:
            return True
        return False

    def is_parenthese_end(forme_parenthesee):
        """
        Détermine si le dernier caractère (espaces exclus) est une parenthèse fermante.

        :pre: Le paramètre forme_parenthesee est une chaine de caractères.
        :post: Renvoi True si le dernier caractère (espaces exclus) est une parenthèse fermante, False sinon.
        """
        new_form = forme_parenthesee.strip()
        if new_form[len(new_form) - 1] == ")":
            return True
        return False

    if type(forme_parenthesee) != str:
        raise ValueError("La forme parenthésée n'est pas une chaîne de caractères.")

    elif forme_parenthesee == "":
        raise ValueError("L'entrée n'est pas en forme parenthésée mais est vide.")

    elif forme_parenthesee.strip() == "":
        raise ValueError("L'entrée n'est pas en forme parenthésée mais ne contient que des espaces.")

    elif not fct_parenthese_number(forme_parenthesee):
        raise ValueError("Parenthèse(s) manquante(s)")

    elif not is_parenthese_end(forme_parenthesee):
        raise ValueError("il y du texte en trop à la fin de la chaîne de caractère.")

##FONCTIONS & CLASSES UTILITAIRES - FIN##


if __name__ == '__main__':
    """
     Ce simple test a pour but de vous permettre de valider plus facilement le résultat de votre
     travail. Ce point d'entrée **ne sera en aucun cas appelée** lors de l'évaluation de votre
     travail sur INGInious. Il est donc nécessaire que vous écriviez votre code en respectant les
     instructions qui vous sont données ci-dessus.
    """
    # Un tableau comprenant des phrases d'exemple.
    example_list  = ["(3 (3 (3 (2 An) (3 (4 engaging) (2 overview))) (2 (2 of) (2 (2 (2 Johnson) (2 's)) (2 (2 eccentric) (3 career))))) (2 .))",
                     "(3 (2 It) (4 (4 (2 's) (4 (3 (2 a) (4 (3 lovely) (2 film))) (3 (2 with) (4 (3 (3 lovely) (2 performances)) (2 (2 by) (2 (2 (2 Buy) (2 and)) (2 Accorsi))))))) (2 .)))",
                     "(2 (2 (1 No) (2 one)) (1 (1 (2 goes) (2 (1 (2 (2 unindicted) (2 here)) (2 ,)) (2 (2 which) (3 (2 (2 is) (2 probably)) (3 (2 for) (4 (2 the) (4 best))))))) (2 .)))",
                     "(3 (2 And) (4 (3 (2 if) (1 (2 you) (1 (2 (2 (2 're) (1 not)) (2 nearly)) (4 (3 (3 moved) (2 (2 to) (1 tears))) (2 (2 by) (2 (2 (2 a) (2 couple)) (2 (2 of) (2 scenes)))))))) (2 (2 ,) (2 (2 you) (2 (2 (2 've) (1 (2 got) (2 (3 (2 ice) (2 water)) (2 (2 in) (2 (2 your) (2 veins)))))) (2 .))))))",
                     "(4 (4 (2 A) (4 (3 (3 warm) (2 ,)) (3 funny))) (3 (2 ,) (3 (4 (4 engaging) (2 film)) (2 .))))",
                     "(4 (3 (2 Uses) (3 (3 (4 (3 sharp) (4 (3 (4 humor) (2 and)) (2 insight))) (2 (2 into) (3 (2 human) (2 nature)))) (2 (2 to) (2 (2 examine) (2 (2 class) (1 conflict)))))) (2 (2 ,) (2 (2 (2 adolescent) (2 (2 (2 yearning) (2 ,)) (3 (2 (2 the) (2 roots)) (3 (2 of) (2 (2 friendship) (2 (2 and) (2 (2 sexual) (2 identity)))))))) (2 .))))",
                     "(2 (2 (2 Half) (1 (2 (2 (2 (2 (2 Submarine) (2 flick)) (2 ,)) (2 (2 Half) (2 (2 Ghost) (2 Story)))) (2 ,)) (2 (2 All) (2 (2 in) (2 (2 one) (2 criminally)))))) (1 (1 neglected) (2 film)))",
                     "(3 (3 (3 Entertains) (3 (2 by) (3 (2 providing) (3 (3 good) (3 (2 ,) (2 (3 lively) (2 company))))))) (2 .))",
                     "(4 (4 (4 Dazzles) (4 (2 with) (4 (3 (4 (4 (3 (3 (2 its) (3 (2 fully-written) (2 characters))) (2 ,)) (2 (2 its) (2 (2 determined) (3 stylishness)))) (2 (1 -LRB-) (3 (2 (2 which) (3 (2 always) (3 (2 relates) (2 (2 to) (2 (2 (2 characters) (2 and)) (2 story)))))) (3 -RRB-)))) (2 and)) (4 (3 (2 (2 Johnny) (2 (2 Dankworth) (2 's))) (4 (4 best) (2 soundtrack))) (2 (2 in) (2 years)))))) (2 .))",
                     "(4 (4 (3 (3 (3 (3 (3 Visually) (4 imaginative)) (2 ,)) (3 (3 thematically) (3 instructive))) (2 and)) (4 (2 thoroughly) (4 delightful))) (3 (2 ,) (3 (2 it) (4 (3 (4 (2 (2 takes) (3 us)) (3 (2 on) (3 (3 (2 a) (3 (2 roller-coaster) (2 ride))) (2 (2 from) (2 innocence))))) (2 (2 to) (2 (2 experience) (3 (2 without) (2 (2 (2 even) (3 (2 a) (3 hint))) (2 (2 of) (2 (2 that) (1 (1 typical) (1 (2 kiddie-flick) (2 sentimentality)))))))))) (2 .)))))",
                     "(3 (3 (1 (2 (2 Nothing) (1 (2 's) (1 (2 at) (1 (2 (2 stake) (2 ,)) (1 (1 (2 just) (3 (2 a) (2 (2 twisty) (2 double-cross)))) (1 (2 you) (1 (2 can) (2 (2 (2 smell) (2 (2 a) (2 mile))) (2 away))))))))) (2 --)) (3 (2 still) (4 (2 ,) (3 (2 (2 the) (2 (3 derivative) (3 (2 Nine) (2 Queens)))) (3 (2 is) (3 (2 lots) (3 (2 of) (4 fun)))))))) (2 .))",
                    ]

    for number, example in enumerate(example_list):
        # Création d'un arbre d'analyse correspondant à l'exemple donné ci-dessus.
        arbre = creer_arbre(example)

        # Impression du résultat de la méthode simplifier en modifiant la variable 'niveau'.
        print(("-" * 10) + ("Phrase n°{}".format(number+1)) + ("-" * 10))
        for niveau in range(5):
            print("Niveau {} - {}".format(niveau, arbre.simplifier(niveau)))
        print("\n")
